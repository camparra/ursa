#!/bin/bash

GITLABDIR="$HOME/srv/gitlab"

for dir in "$GITLABDIR/config" "$GITLABDIR/logs" "$GITLABDIR/data"
do
    if [ ! -d $dir ] ; then
        mkdir -p $dir
        chmod -R 777 $dir
    fi
done

docker run --detach \
 --hostname gitlab.sovrin.org \
 --publish 443:443 \
 --publish 80:80 \
 --publish 22:22 \
 --name gitlab \
 --restart always \
 --volume $GITLABDIR/config:/etc/gitlab \
 --volume $GITLABDIR/logs:/var/log/gitlab \
 --volume $GITLABDIR/data:/var/opt/gitlab \
 gitlab/gitlab-ce:latest
